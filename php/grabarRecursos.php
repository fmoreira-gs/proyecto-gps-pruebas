<?php
    require_once __DIR__ . '/sql/cone-sql.php';
    require_once __DIR__ . '/config.php';

    //SE CONVIERTE EN ARRAY EL JSON RECIBIDO
    $recursos = json_decode($_POST['recursos'], true);
    $mensaje="";

    //SE ELIMINA TODOS LOS REGISTROS DE LA TABLA PARA INGRESAR REGISTROS ACTUALES
    eliminarDatos(Config::$tablaRecursos,"");
    eliminarDatos(Config::$tablaVinculacion,"");

    //INGRESO DE DATOS 
    foreach($recursos as $row){
        $idRecurso = $row['_id'];
        $nombre = $row['$$user_name'];
        insertarDatos(Config::$tablaRecursos, "idRecurso, nomRecurso", "$idRecurso,'$nombre'");
        //$mensaje .= "$idUnidad, '$nombre'\n";
        $notificaciones =  $row['$$user_notifications'];
        foreach($notificaciones as $noti){
            $idZona = $noti['id'];
            $unidades = $noti['un'];
            foreach($unidades as $un){
                insertarDatos(Config::$tablaVinculacion, "idUnidad, idRecurso, idZona", "$un,$idRecurso,$idZona");
                $mensaje .=  $idRecurso . "- " . $idZona . "- " . $un ."\n";
            }
        }
    }
    echo $mensaje;
?>
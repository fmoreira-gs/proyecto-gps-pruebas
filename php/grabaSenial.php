<?php
    require_once __DIR__ . '/sql/cone-sql.php';
    require_once __DIR__ . '/config.php';

    $evento = $_POST['evento'];
    $unidad = $_POST['unidad'];
    $lat = $_POST['latitud'];
    $lon = $_POST['longitud'];
    $hora = $_POST['hora'];
    $idSNL = $_POST['id'];
    $idRecurso = $_POST['idRecurso'];

    $codigo="";
    $codigoSNL="";

    //OBTENGO EL CODIGO DE LA SE;AL DE ACUERDO AL ID DE NOTIFICACION RECIBIDA
    $result = retornaDatos(Config::$tablaNotificaciones,"top (1) codigoSenial", "idZona=$idSNL and idRecurso=$idRecurso","");
    while($row = $result->fetch(PDO::FETCH_ASSOC)){
        $codigoSNL = $row['codigoSenial'];
    }

    if(strlen($idSNL) == 2){
        $idSNL = '0' . $idSNL;
    }elseif(strlen($idSNL) == 1){
        $idSNL = '00' . $idSNL;
    }

    //OBTENGO EL CODIGO DE MONITOREO SEGUN EL ID DE UNIDAD RECOGIDO
    $result = retornaDatos("GPS_vinculacionUnidadCodigo","top (1) codigo", "idUnidad=$unidad","");
    while($row = $result->fetch(PDO::FETCH_ASSOC)){
        $codigo = $row['codigo'];
    }

    if($codigo != ""){
        $campos = "cuenta, evento, codigoSenial, zona, idSenial, fecha";
        //GENERO EL NUMERO ALEATORIO PARA LA SENIAL
        $aleatorio = mt_rand(1,65565);

        $valores = "'$codigo','E','$codigoSNL','$idSNL',$aleatorio,GETDATE()";
        insertarDatos(Config::$tablaSeniales, $campos, $valores);
    }
?> 

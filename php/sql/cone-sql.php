<?php
    require_once __DIR__ . '/../config.php';
    function conec(){
        try{
            $conn = new PDO('sqlsrv:Server=' . Config::$server . ';Database=' . Config::$database . '','' . Config::$user . '','' . Config::$pwd . '');
    
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
 
            return $conn;
 
        }catch(PDOException $e){
            die("Error Al Conectar " . $e->getMessage());
        }
    }

    function insertarDatos($tabla, $campos, $valores){
        try{
            $SQL = "INSERT INTO $tabla( $campos ) VALUES( $valores )";
            echo $SQL;
            $cnn = conec();
            $result = $cnn->prepare($SQL);
            return $result->execute();
        }catch(PDOException $e){
            return $e.getMessage();
        }
    }

    function editarDatos($tabla, $campos, $condicion){
        try{
            $SQL = "UPDATE $tabla SET $campos WHERE $condicion";
            echo($SQL);
            $cnn = conec();
            $result = $cnn->prepare($SQL);
            return $result->execute();
        }catch(PDOException $e){
            return $e.getMessage();
        }
    }

    function eliminarDatos($tabla, $condicion){
        try{
            if($condicion != ""){
                $where = ' WHERE ' . $condicion;
            }else{
                $where = "";
            }
            $SQL = "DELETE FROM $tabla $where";
            $cnn = conec();
            $result = $cnn->prepare($SQL);
            return $result->execute();
        }catch(PDOException $e){
            return $e.getMessage();
        }
    }

    function retornaDatos($tabla,$campos,$condicion,$orden){
        try{
            $orderBy="";
            $where="";
            if ($orden != ''){
                $orderBy = ' ORDER BY ' . $orden;
            }
            if($condicion != ''){
                $where = ' WHERE ' . $condicion;
            }

            $SQL= "SELECT $campos FROM " . $tabla .  $where . $orderBy;
            echo $SQL;
            $conn = conec();
            //$conn->query("SET NAMES 'UTF8' ");
            $result = $conn->query($SQL);
            return $result;
        }catch(PDOException $e){
            echo $e.getMessage();
            return $e.getMessage();
        }
    }

?>
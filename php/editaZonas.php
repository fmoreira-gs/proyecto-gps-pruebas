<?php
    require_once __DIR__ . '/sql/cone-sql.php';
    require_once __DIR__ . '/config.php';

    //SE CONVIERTE EN ARRAY EL JSON RECIBIDO
    $zonas = json_decode($_POST['zona'], true);
    $datos = json_decode($_POST['data'], true);

    $idRecurso = $_POST['id'];

    $mensaje="";

    //TIPO DE ACCION QUE SE REALIZO
    if(!empty($datos)){
        switch ($datos['p']['action']) {
            case 'create_zone':
                print_r('create_zone');
                break;
            case 'update_zone':
                print_r('update_zone');
                break;
            case 'delete_zone':
                print_r('delete_zone');
                break;
            case 'create_notify':
                $newNom = $datos['p']['p1'];
                foreach($zonas as $row){
                    $id = $row['id'];
                    $nombre = $row['n'];
                    if($newNom == $nombre){
                        $codSenial = substr($nombre,0,3);
                        $result = retornaDatos("Plantilla_Zonas","Senial"," Senial='$codSenial' AND Formato ='GPS'",$orden);
                        if($result->fetchAll()){
                            $codigoSenial ="'$codSenial'";
                        }else {
                            $codigoSenial = 'NULL';
                        }
                        $campos = "idZona, idRecurso, descripcion, codigoSenial";
                        $valores = "$id, $idRecurso, '$nombre', $codigoSenial";
                        insertarDatos(Config::$tablaNotificaciones, $campos, $valores);
                        $mensaje .= "Se creo la notificacion: $id    $idRecurso  $nombre $codSenial\n";
                     }
                }
                break;
            case 'update_notify':
                $newNom = $datos['p']['p1'];
                foreach($zonas as $row){
                    $id = $row['id'];
                    $nombre = $row['n'];
                    if($newNom == $nombre){
                        $codSenial = substr($nombre,0,3);
                        $result = retornaDatos("Plantilla_Zonas","Senial"," Senial='$codSenial' AND Formato ='GPS'",$orden);
                        if($result->fetchAll()){
                            $codigoSenial ="'$codSenial'";
                        }else {
                            $codigoSenial = 'NULL';
                        }
                        editarDatos(Config::$tablaNotificaciones, "descripcion = '$nombre', codigoSenial=$codigoSenial", "idZona=$id and idRecurso=$idRecurso");
                        $mensaje .= "Se actualizo la lista de Notificacion: $id $idRecurso  $nombre $codSenial\n";
                    }
                }
                break;
            case 'delete_notify':
                $nombre = $datos['p']['p1'];
                eliminarDatos(Config::$tablaNotificaciones," descripcion='$nombre' and idRecurso=$idRecurso");
                $mensaje = "Se elimino la Notificacion '$nombre'\n";
                break;
        }
    }else{
        foreach($zonas as $row){
            $id = $row['id'];
            $nombre = $row['n'];
            editarDatos(Config::$tablaNotificaciones, "descripcion = '$nombre'", "idZona=$id and idRecurso=$idRecurso");
            $mensaje .= "$id    $idRecurso  $nombre\n";
        }
    }
    echo ($mensaje);
?>
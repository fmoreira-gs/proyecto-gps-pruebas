<?php
    require_once __DIR__ . '/sql/cone-sql.php';
    require_once __DIR__ . '/config.php';

    //SE CONVIERTE EN ARRAY EL JSON RECIBIDO
    $unidades = json_decode($_POST['unidad'], true);
    $grupos = json_decode($_POST['grupos'], true);
    $mensaje="";

    //SE ELIMINA TODOS LOS REGISTROS DE LA TABLA PARA INGRESAR REGISTROS ACTUALES
    eliminarDatos(Config::$tablaUnidades,"");

    //INGRESO DE DATOS 
    foreach($unidades as $row){
        $idUnidad = $row['_id'];
        $nombre = $row['$$user_name'];
        insertarDatos(Config::$tablaUnidades, "idUnidad, nombre, tipoDispositivo, idUnico", "$idUnidad,'$nombre', NULL,NULL");

        //$mensaje .= "$idUnidad, '$nombre'\n";
    }


    //SE ELIMINA TODOS LOS REGISTROS DE LA TABLA PARA INGRESAR REGISTROS ACTUALES
    eliminarDatos(Config::$tablaGrupos,"");
    eliminarDatos(Config::$tablaUnidadGrupo,"");
   
    //INGRESO DE DATOS 
    foreach($grupos as $rowGroup){
        $idGrupo = $rowGroup['_id'];
        $descripcion = $rowGroup['$$user_name'];
        insertarDatos(Config::$tablaGrupos, "idGrupo, descripcion", "$idGrupo,'$descripcion'");

        $detalleUnidades = $rowGroup['$$user_units'];
        foreach($detalleUnidades as $un){
            insertarDatos(Config::$tablaUnidadGrupo, "idGrupo, idUnidad", "$idGrupo,$un");
            $mensaje .=  $idGrupo . "- " . $un ."\n";
        }
    }
    echo $mensaje;

?>
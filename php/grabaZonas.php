<?php
    require_once __DIR__ . '/sql/cone-sql.php';
    require_once __DIR__ . '/config.php';

    //SE CONVIERTE EN ARRAY EL JSON RECIBIDO
    $zonas = json_decode($_POST['zona'], true);
    $datos = json_decode($_POST['data'], true);

    $idRecurso = $_POST['id'];

    if($idRecurso != 20264273 && $idRecurso != 20743254){
        //SE ELIMINA TODOS LOS REGISTROS DE LA TABLA PARA INGRESAR REGISTROS ACTUALES
        eliminarDatos(Config::$tablaNotificaciones," idRecurso <> 20264273 and idRecurso <> 20743254 ");

        foreach($zonas as $row){
            $id = $row['id'];
            $nombre = $row['n'];
            insertarDatos(Config::$tablaNotificaciones, "idZona, idRecurso, descripcion", " $id, $idRecurso, '$nombre'");

            $mensaje .= "$id, $idRecurso '$nombre'\n";
        }
    }
    echo $mensaje;
?>
// Print message to log
function msg(text) { $("#log").prepend(text + "<br/>"); }
var data="";
var units="";
var unitsGroups="";

function init() { // Ejecutar después de iniciar sesión correctamente
	var sess = wialon.core.Session.getInstance(); // obtener instancia de la sesión actual
    //especifique qué tipo de datos deben devolverse (indicadores diferentes para 'avl_unit' y 'avl_resource')
	var flags = wialon.item.Item.dataFlag.base 
				| wialon.item.Resource.dataFlag.base 
				| wialon.item.Item.dataFlag.messages 
				| wialon.item.Resource.dataFlag.notifications 
				| wialon.item.Resource.dataFlag.zones
				| wialon.item.Resource.dataFlag.zoneGroups
				| wialon.item.Unit.dataFlag.data
				| wialon.item.Unit.dataFlag.lastMessage
				| wialon.item.Item.dataFlag.adminFields;

	sess.loadLibrary("resourceZones"); 
	sess.loadLibrary("resourceUnits"); 
	sess.loadLibrary("resourceNotifications"); // cargar biblioteca de notificaciones
	sess.loadLibrary("itemCustomFields"); // IMPORTANT! Para la carga de campos personalizados se necesita la libreria "itemCustomFields"
	sess.loadLibrary("itemProfileFields");
	sess.loadLibrary("itemIcon");

    sess.updateDataFlags( //cargar elementos a la sesión actual
	[{type: "type", data: "avl_resource", flags: flags, mode: 0},
	{type: "type", data: "avl_unit", flags: flags, mode: 0},
	{type: "type", data: "user", flags: flags, mode: 0},
	{type: "type", data: "avl_retranslator", flags: flags, mode: 0},
	{type: "type", data: "avl_unit_group", flags: flags, mode: 0}], // Especificación de elementos (avl_resource)
	function (code) { // devolución de llamada updateDataFlags
		if (code) { msg(wialon.core.Errors.getErrorText(code)); return; } // exit if error code 
		var res = sess.getItems("avl_resource");
		grabarRecursos(res);
		for (var i = 0; i< res.length; i++) {// construir una lista de Seleccion utilizando recursos encontrados
			$("#res").append("<option value='"+ res[i].getId() +"'>"+ res[i].getName() +"</option>");	
			//addEvent(res[i].getId()); // add event to any resource object
			res[i].addListener("messageRegistered", showData); // registrarse evento cuando recibiremos mensaje
			actualizaZonas(res[i].getId());
		}
		//CODIGO PARA ALMACENAR LAS UNIDADES Y GRUPOS 
		units = sess.getItems("avl_unit");

		unitsGroups = sess.getItems("avl_unit_group");
		grabarUnidades();
	});
	
	$("#count").text(0); // count of notification
	$("#close_popup").click(close_popup); // event on close popup block
	$("#notification").on("click", ".close_btn", delete_info); // evento en eliminar fila con información de notificación
	
	//console.log(JSON.stringify(sess));
}

function grabarRecursos(recurso){
	//console.log(JSON.stringify(units));
	var json = {
		recursos : JSON.stringify(recurso)
	}

	$.ajax({
		url : "php/grabarRecursos.php",
		type: "POST",
		data: json,
		dataType : "text",
		success : function(r){
			//console.log(r);
		}
	});
}

function getZones(res_id){ // get geofences by resource id
	if(res_id){ // check if 
		var res = wialon.core.Session.getInstance().getItem(res_id);
		var zones = res.getNotifications(); // get resource's zones
		for(var i in zones) // construct Select list using found zones
			$("#zones").append("<option value='"+ res_id +"_"+ zones[i].id +"_"+ zones[i].n +"'>"+ res_id + '-' + zones[i].id +"-"+ zones[i].n +"</option>");
			actualizaZonas(res_id);
	}
}

function showData(event) {
	data = event.getData(); // obtener datos del evento
	recursoId = event._target._id;
	ahora = new Date();

	hora = ahora.getHours();
	minuto = ahora.getMinutes();
	seg =  ahora.getSeconds();
	horaActual= hora + ":" + minuto + ":" + seg;

	if (data.tp && data.tp == "unm") {
		if($("#count").text() == 10){
			$("#count").text(0);
			$("#notification").empty();
			$("#notification").append("<tr><td>id Unid</td><td>id Señal</td><td>Name</td><td>FechaHora</td><td>Text</td><td>Delete</td><tr>"); // add row with data to info-table
			$("#count").text(parseInt($("#count").text()) + 1);
		}else{
			$("#count").text(parseInt($("#count").text()) + 1); // get notification count
			$("#container").show();
		}
		$("#notification").append("<tr><td>" + data.unit + "</td><td>" + data.nid + "</td><td>" + data.name + "</td><td>" + horaActual + "</td><td>" + data.txt + "</td><td id='" + data.t + "' class='close_btn'>x</td><tr>"); // add row with data to info-table
		grabaSenial(data, recursoId);
	}

	if(data.p.action && data.tp == "xx"){
		//console.log(JSON.stringify(data));
		var timerID = setInterval(function(){
			cargaRecursos(timerID);
		},2000);
	}
}

function cargaRecursos(timerID){
	//SE ELIMINA EL INTERVALO
	clearInterval(timerID);
	var session = wialon.core.Session.getInstance();
	var res = session.getItems("avl_resource");
	grabarRecursos(res);
	grabarUnidades();
	//var res = session.getItem(res_id); // get resource by id
	$('#zones').empty();
	for (var i = 0; i< res.length; i++) { 
		getZones(res[i].getId());
	}
}

function grabaSenial(data, recursoId){
	var jsonData = {
		evento : data.txt,
		unidad : data.unit,
		latitud : data.x,
		longitud : data.y,
		hora : horaActual,
		id : data.nid,
		idRecurso : recursoId
	};
	
	$.ajax({
		url : "php/grabaSenial.php",
		type: "POST",
		data: jsonData,
		dataType : "text",
	});
}

function actualizaZonas(idR){
	var res = wialon.core.Session.getInstance().getItem(idR);
	var zonas = res.getNotifications(); // get resource's zones
	//console.log(JSON.stringify(zonas));
	var json = {
		zona : JSON.stringify(zonas),
		id : idR,
		data : JSON.stringify(data)
	}

	$.ajax({
		url : "php/editaZonas.php",
		type: "POST",
		data: json,
		dataType : "text",
		success : function(r){
			//console.log(r);
		}
	});
}

function grabarUnidades(){
	//console.log(JSON.stringify(units));
	var json = {
		unidad : JSON.stringify(units),
		grupos : JSON.stringify(unitsGroups)
	}

	$.ajax({
		url : "php/grabaUnidades.php",
		type: "POST",
		data: json,
		dataType : "text",
		success : function(r){
			//console.log(r);
		}
	});
}

function close_popup() {
	$("#container").hide();
}

function delete_info(event) {
	// delete notification from list
	$(event.target.parentNode).remove();
}

// ejecutar cuando DOM esté listo when DOM ready
$(document).ready(function () {
	login();
});

function login(){
	wialon.core.Session.getInstance().initSession("https://hst-api.wialon.com"); // init session
    // For more info about how to generate token check
    // http://sdk.wialon.com/playground/demo/app_auth_token
	wialon.core.Session.getInstance().loginToken("9c4aae53c8217637c6814b4f3cbad3913802F437A3046684CDEE1035B5DF572E9777BC5B", "", // try to login
	    function (code) { // login callback
	    	if (code){ 
				msg(wialon.core.Errors.getErrorText(code)); // exit if error code
				return;  
			} 
	    	msg("Logged successfully"); 
			init();// when login suceed then run init() function
		}
	);
}

function logout(){
	wialon.core.Session.getInstance().logout( // cerrar session
		function (code) { // login callback
	    	if (code){ 
				msg(wialon.core.Errors.getErrorText(code)); // exit if error code
				return;  
			} 
	    	msg("Sesion Cerrada con Exito"); 
		}
	); 
}

//Funcion para recargar la página y los recursos
function actualizar(){location.reload(true);}
